#!/bin/bash
set -Eeuo pipefail
# no shopt -s inherit_errexit as this needs to run on the ancient Bash in RHEL6

# shellcheck source-path=SCRIPTDIR
source common_functions.sh

RPM_PACKAGE_MANAGER="yum"
RPM_PACKAGES=(
    gcc
    perl
    tar
)
SOURCES_DIR="/usr/src"
ARTIFACT_NAME="sqlite"
ARTIFACT_SRC="/usr/local"
# shellcheck disable=SC2154
NORMALIZED_VERSION=$(set -e; sqlite_normalized_version "${SQLITE_VERSION}")

install_packages "${RPM_PACKAGE_MANAGER}" "${RPM_PACKAGES[@]}"
download_and_uncompress_tgz "https://www.sqlite.org/2022/sqlite-autoconf-${NORMALIZED_VERSION}.tar.gz" "${SOURCES_DIR}"
cd "${SOURCES_DIR}/sqlite-autoconf-${NORMALIZED_VERSION}"
configure_and_install ""
store_artifact "${ARTIFACT_NAME}" "${ARTIFACT_SRC}"
